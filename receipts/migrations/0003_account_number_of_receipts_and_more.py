# Generated by Django 5.0.3 on 2024-03-14 15:53

from django.db import migrations, models


class Migration(migrations.Migration):

    dependencies = [
        ("receipts", "0002_alter_receipt_date"),
    ]

    operations = [
        migrations.AddField(
            model_name="account",
            name="number_of_receipts",
            field=models.IntegerField(null=True),
        ),
        migrations.AddField(
            model_name="expensecategory",
            name="number_of_receipts",
            field=models.IntegerField(null=True),
        ),
        migrations.AlterField(
            model_name="receipt",
            name="date",
            field=models.DateTimeField(),
        ),
    ]
