CREATE TABLE IF NOT EXISTS "receipts_receipt" (
    "id" integer NOT NULL PRIMARY KEY AUTOINCREMENT, 
    "vendor" varchar(200) NOT NULL, 
    "total" decimal NOT NULL, 
    "tax" decimal NOT NULL, 
    "account_id" bigint NULL REFERENCES "receipts_account" ("id") DEFERRABLE INITIALLY DEFERRED, 
    "category_id" bigint NOT NULL REFERENCES "receipts_expensecategory" ("id") DEFERRABLE INITIALLY DEFERRED, 
    "purchaser_id" integer NOT NULL REFERENCES "auth_user" ("id") DEFERRABLE INITIALLY DEFERRED, 
    "date" date NOT NULL);
CREATE INDEX "receipts_receipt_account_id_7c5e326b" ON "receipts_receipt" ("account_id");
CREATE INDEX "receipts_receipt_category_id_3ba12aba" ON "receipts_receipt" ("category_id");
CREATE INDEX "receipts_receipt_purchaser_id_0e1af242" ON "receipts_receipt" ("purchaser_id");